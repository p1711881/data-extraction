<!DOCTYPE article
PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD with MathML3 v1.3 20210610//EN" "JATS-archivearticle1-3-mathml3.dtd">
<article xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML" article-type="case-report" dtd-version="1.3" xml:lang="fr"><?properties open_access?><processing-meta base-tagset="archiving" mathml-version="3.0" table-model="xhtml" tagset-family="jats"><restricted-by>pmc</restricted-by></processing-meta><front><journal-meta><journal-id journal-id-type="nlm-ta">Rev Rhum Ed Fr</journal-id><journal-id journal-id-type="iso-abbrev">Rev Rhum Ed Fr</journal-id><journal-title-group><journal-title>Revue du Rhumatisme (Ed. Francaise : 1993)</journal-title></journal-title-group><issn pub-type="ppub">1169-8330</issn><issn pub-type="epub">1768-3130</issn><publisher><publisher-name>Soci&#x000e9;t&#x000e9; Fran&#x000e7;aise de Rhumatologie. Published by Elsevier Masson SAS.</publisher-name></publisher></journal-meta><article-meta><article-id pub-id-type="pmid">34248353</article-id><article-id pub-id-type="pmc">8262390</article-id><article-id pub-id-type="pii">S1169-8330(21)00209-X</article-id><article-id pub-id-type="doi">10.1016/j.rhum.2021.07.003</article-id><article-categories><subj-group subj-group-type="heading"><subject>Fait Clinique</subject></subj-group></article-categories><title-group><article-title>N&#x000e9;vralgie amyotrophiante et infection &#x000e0; COVID-19&#x000a0;: deux cas de paralysie du nerf spinal accessoire<sup><xref ref-type="fn" rid="d36e398">&#x02606;</xref></sup></article-title></title-group><contrib-group><contrib contrib-type="author" id="aut0005"><name><surname>Coll</surname><given-names>Clemence</given-names></name><xref rid="aff0005" ref-type="aff">a</xref></contrib><contrib contrib-type="author" id="aut0010"><name><surname>Tessier</surname><given-names>Muriel</given-names></name><xref rid="aff0005" ref-type="aff">a</xref><xref rid="cor0005" ref-type="corresp">&#x0204e;</xref></contrib><contrib contrib-type="author" id="aut0015"><name><surname>Vandendries</surname><given-names>Christophe</given-names></name><xref rid="aff0010" ref-type="aff">b</xref><xref rid="aff0015" ref-type="aff">c</xref></contrib><contrib contrib-type="author" id="aut0020"><name><surname>Seror</surname><given-names>Paul</given-names></name><xref rid="aff0020" ref-type="aff">d</xref><xref rid="aff0025" ref-type="aff">e</xref></contrib><aff id="aff0005"><label>a</label>Service de soins de suites et de r&#x000e9;adaptation, h&#x000f4;pital Robert-Ballanger, boulevard Robert-Ballanger, 93602 Aulnay-sous-Bois, France</aff><aff id="aff0010"><label>b</label>Service de radiologie, fondation ophtalmologique de Rothschild, 29, rue Manin, 75019 Paris, France</aff><aff id="aff0015"><label>c</label>Centre Medical RMX, 80, avenue Felix Faure, 75015 Paris, France</aff><aff id="aff0020"><label>d</label>Cabinet d&#x02019;&#x000e9;lectroneuromyographie, 146, avenue Ledru Rollin, 75011 Paris, France</aff><aff id="aff0025"><label>e</label>Hopital Priv&#x000e9; de l&#x02019;Est Parisien, 93600 Aulnay-sous-Bois, France</aff></contrib-group><author-notes><corresp id="cor0005"><label>&#x0204e;</label>Auteur correspondant.</corresp></author-notes><pub-date pub-type="pmc-release"><day>7</day><month>7</month><year>2021</year></pub-date><!-- PMC Release delay is 0 months and 0 days and was based on <pub-date
						pub-type="epub">.--><pub-date pub-type="ppub"><month>1</month><year>2022</year></pub-date><pub-date pub-type="epub"><day>7</day><month>7</month><year>2021</year></pub-date><volume>89</volume><issue>1</issue><fpage>92</fpage><lpage>95</lpage><history><date date-type="accepted"><day>12</day><month>4</month><year>2021</year></date></history><permissions><copyright-statement>&#x000a9; 2021 Soci&#x000e9;t&#x000e9; Fran&#x000e7;aise de Rhumatologie. Published by Elsevier Masson SAS. All rights reserved.</copyright-statement><copyright-year>2021</copyright-year><copyright-holder>Soci&#x000e9;t&#x000e9; Fran&#x000e7;aise de Rhumatologie</copyright-holder><license><license-p>Since January 2020 Elsevier has created a COVID-19 resource centre with free information in English and Mandarin on the novel coronavirus COVID-19. The COVID-19 resource centre is hosted on Elsevier Connect, the company's public news and information website. Elsevier hereby grants permission to make all its COVID-19-related research that is available on the COVID-19 resource centre - including this research content - immediately available in PubMed Central and other publicly funded repositories, such as the WHO COVID database with rights for unrestricted research re-use and analyses in any form or by any means with acknowledgement of the original source. These permissions are granted for free by Elsevier for as long as the COVID-19 resource centre remains active.</license-p></license></permissions><abstract id="abs0010"><sec><title>Objectif</title><p>La n&#x000e9;vralgie amyotrophiante de Parsonage et Turner (NAPT) appara&#x000ee;t souvent suite &#x000e0; un stress m&#x000e9;canique loco-r&#x000e9;gional ou &#x000e0; une infection virale. Nous rapportons deux cas d&#x02019;amyotrophie et d&#x000e9;ficit de l&#x02019;&#x000e9;paule li&#x000e9;s &#x000e0; une paralysie du nerf spinal accessoire (NSA), caus&#x000e9;e par une NAPT dans les suites d&#x02019;une infection &#x000e0; COVID-19.</p></sec><sec><title>M&#x000e9;thodes</title><p>Pour les deux patients concern&#x000e9;s, l&#x02019;interrogatoire, l&#x02019;examen clinique et les examens compl&#x000e9;mentaires (imageries, examens biologiques et &#x000e9;lectroneuromyogramme (ENMG)) ont permis de confirmer le diagnostic de NAPT et d&#x02019;infirmer les diagnostics diff&#x000e9;rentiels.</p></sec><sec><title>R&#x000e9;sultats</title><p>La NAPT ne concernait que le nerf spinal accessoire dans les deux cas rapport&#x000e9;s. L&#x02019;ENMG a mis en &#x000e9;vidence des l&#x000e9;sions axonales r&#x000e9;centes caract&#x000e9;ristiques de la NAPT. Les IRM des deux patients ont r&#x000e9;v&#x000e9;l&#x000e9; un hypersignal T2&#x000a0;de d&#x000e9;nervation dans les muscles atteints. Aucun signe de masse, kyste, l&#x000e9;sion, bride ou d&#x000e9;chirure n&#x02019;a &#x000e9;t&#x000e9; retrouv&#x000e9; le long du NSA.</p></sec><sec><title>Conclusion</title><p>Comme d&#x02019;autres viroses, la COVID-19&#x000a0;pourrait &#x000ea;tre le facteur d&#x000e9;clenchant de la NAPT, comme elle semble aussi &#x000ea;tre un facteur d&#x000e9;clenchant possible du syndrome de Guillain Barr&#x000e9;.</p></sec></abstract><kwd-group id="kwd0005"><title>Mots cl&#x000e9;s</title><kwd>COVID-19</kwd><kwd>N&#x000e9;vralgie amyotrophiante</kwd><kwd>Syndrome de Parsonage et Turner</kwd><kwd>Nerf spinal accessoire</kwd><kwd>Paralysie du muscle trap&#x000e8;ze</kwd><kwd>Neuropathie p&#x000e9;riph&#x000e9;rique</kwd></kwd-group></article-meta></front><body><sec id="sec0005"><label>1</label><title>Introduction</title><p id="par0030">Les principales manifestations de la COVID-19&#x000a0;sont respiratoires, la principale forme grave est le syndrome respiratoire aigu s&#x000e9;v&#x000e8;re (SRAS) <xref rid="bib0075" ref-type="bibr">[1]</xref>. Des manifestations neurologiques ont &#x000e9;galement &#x000e9;t&#x000e9; d&#x000e9;crites, impliquant le syst&#x000e8;me nerveux central, mais aussi le syst&#x000e8;me nerveux p&#x000e9;riph&#x000e9;rique, allant de l&#x02019;anosmie, fr&#x000e9;quente et b&#x000e9;nigne, aux formes graves du syndrome de Guillan Barr&#x000e9; (SGB) <xref rid="bib0075" ref-type="bibr">[1]</xref>, <xref rid="bib0080" ref-type="bibr">[2]</xref>. La n&#x000e9;vralgie amyotrophiante de Parsonage et Turner (NAPT) correspond &#x000e0; une neuropathie axonale p&#x000e9;riph&#x000e9;rique monophasique, &#x000e0; d&#x000e9;but brutal et douloureux, entra&#x000ee;nant&#x000a0;: faiblesse, amyotrophie et d&#x000e9;ficit sensitif dans le territoire du nerf atteint. Les l&#x000e9;sions nerveuses peuvent &#x000ea;tre uniques ou multiples. Elles sont g&#x000e9;n&#x000e9;ralement de distribution asym&#x000e9;trique et al&#x000e9;atoire, impliquant pr&#x000e9;f&#x000e9;rentiellement les membres sup&#x000e9;rieurs <xref rid="bib0085" ref-type="bibr">[3]</xref>. &#x000c0; l&#x02019;instar du SGB, la physiopathologie de la NAPT serait auto-immune et inflammatoire. Elle serait g&#x000e9;n&#x000e9;ralement d&#x000e9;clench&#x000e9;e par un stress m&#x000e9;canique ou une infection virale <xref rid="bib0085" ref-type="bibr">[3]</xref>, <xref rid="bib0090" ref-type="bibr">[4]</xref>
<italic>.</italic> Trois cas de NAPT li&#x000e9;s &#x000e0; des infections respiratoires &#x000e0; COVID-19&#x000a0;ont &#x000e9;t&#x000e9; d&#x000e9;crits depuis le d&#x000e9;but de la pand&#x000e9;mie <xref rid="bib0095" ref-type="bibr">[5]</xref>, <xref rid="bib0100" ref-type="bibr">[6]</xref>, <xref rid="bib0105" ref-type="bibr">[7]</xref>. L&#x02019;un d&#x02019;entre eux &#x000e9;tait purement sensitif <xref rid="bib0095" ref-type="bibr">[5]</xref>, le second impliquait les muscles sus-&#x000e9;pineux, sous-&#x000e9;pineux, petit rond, grand rond et trap&#x000e8;ze <xref rid="bib0100" ref-type="bibr">[6]</xref>, et le troisi&#x000e8;me impliquait le nerf m&#x000e9;dian <xref rid="bib0105" ref-type="bibr">[7]</xref>. Ce travail rapporte deux cas de NAPT impliquant le nerf spinal accessoire (NSA) survenus dans les suites d&#x02019;un SRAS document&#x000e9; &#x000e0; COVID-19.</p></sec><sec id="sec0010"><label>2</label><title>Cas cliniques</title><sec id="sec0015"><label>2.1</label><title>Cas 1</title><p id="par0035">Un patient de 63&#x000a0;ans a pr&#x000e9;sent&#x000e9; un SRAS d&#x000fb; &#x000e0; une infection &#x000e0; COVID-19, document&#x000e9;e par PCR sur &#x000e9;couvillon naso-pharyng&#x000e9; et scanner thoracique. L&#x02019;infection &#x000e0; Coronavirus a &#x000e9;t&#x000e9; trait&#x000e9;e par Hydroxychloroquine, Azithromycine, Baracitinib, Ceftriaxone et Dexamethasone. L&#x02019;aggravation des sympt&#x000f4;mes respiratoires a n&#x000e9;cessit&#x000e9; une hospitalisation en r&#x000e9;animation, pour ventilation m&#x000e9;canique et s&#x000e9;dation pendant 6&#x000a0;semaines&#x000a0;: d&#x02019;abord par intubation oro-trach&#x000e9;ale, puis par trach&#x000e9;otomie percutan&#x000e9;e. &#x000c0; son r&#x000e9;veil, le patient a souffert de troubles de la conscience &#x000e0; type de confusion et agitation, explor&#x000e9;s par ponction lombaire, &#x000e9;lectroenc&#x000e9;phalogramme et imageries neurologiques, sans qu&#x02019;aucune cause ne soit retrouv&#x000e9;e <xref rid="bib0110" ref-type="bibr">[8]</xref>, <xref rid="bib0115" ref-type="bibr">[9]</xref>. Un mois apr&#x000e8;s sa sortie de r&#x000e9;animation,&#x000a0;il s&#x02019;est plaint de douleur et d&#x02019;un d&#x000e9;ficit moteur de l&#x02019;&#x000e9;paule droite, puis de paresth&#x000e9;sies des territoires ulnaires des deux avant-bras. La premi&#x000e8;re &#x000e9;valuation clinique retrouvait une limitation des amplitudes articulaires passives et actives de l&#x02019;&#x000e9;paule ainsi qu&#x02019;une amyotrophie du trap&#x000e8;ze sup&#x000e9;rieur droit et des fosses sus et sous-&#x000e9;pineuses droites <xref rid="bib0120" ref-type="bibr">[10]</xref>, <xref rid="bib0125" ref-type="bibr">[11]</xref>. Le testing musculaire de l&#x02019;&#x000e9;paule a retrouv&#x000e9; un d&#x000e9;ficit moteur pr&#x000e9;dominant en &#x000e9;l&#x000e9;vation lat&#x000e9;rale, avec une dyskin&#x000e9;sie scapulo-thoracique&#x000a0;: la scapula &#x000e9;tait l&#x000e9;g&#x000e8;rement translat&#x000e9;e vers l&#x02019;ext&#x000e9;rieur au repos, avec un glissement brutal lat&#x000e9;ral lors de l&#x02019;&#x000e9;l&#x000e9;vation lat&#x000e9;rale du membre sup&#x000e9;rieur (<xref rid="fig0005" ref-type="fig">Fig. 1</xref>
) <xref rid="bib0125" ref-type="bibr">[11]</xref>, <xref rid="bib0130" ref-type="bibr">[12]</xref>. La force musculaire &#x000e9;tait conserv&#x000e9;e par ailleurs, notamment pour le muscle sterno-cl&#x000e9;&#x000ef;do-masto&#x000ef;dien (SCM). Les r&#x000e9;flexes ost&#x000e9;o-tendineux &#x000e9;taient conserv&#x000e9;s. Le patient souffrait d&#x02019;une capsulite r&#x000e9;tractile ipsilat&#x000e9;rale associ&#x000e9;e, mais sans que cette affection ne permette d&#x02019;expliquer l&#x02019;ensemble du tableau clinique, notamment l&#x02019;importance de l&#x02019;amyotrophie de l&#x02019;&#x000e9;paule. Un &#x000e9;lectroneuromyogramme (ENMG) r&#x000e9;alis&#x000e9; &#x000e0; 5&#x000a0;mois du d&#x000e9;but de l&#x02019;infection virale a d&#x000e9;montr&#x000e9; une atteinte axonale importante et isol&#x000e9;e du nerf spinal accessoire droit&#x000a0;: l&#x02019;&#x000e9;tude des conductions nerveuses a mis en &#x000e9;vidence des latences de conduction normales, avec une diminution importante de l&#x02019;amplitude des potentiels d&#x02019;action moteurs des muscles trap&#x000e8;ze sup&#x000e9;rieur et inf&#x000e9;rieur droits, en comparaison avec le c&#x000f4;t&#x000e9; gauche (<xref rid="fig0010" ref-type="fig">Fig. 2</xref>
). L&#x02019;examen &#x000e0; l&#x02019;aiguille a retrouv&#x000e9; des trac&#x000e9;s fortement neurog&#x000e8;nes et des signes de d&#x000e9;nervation active dans les muscles trap&#x000e8;zes sup&#x000e9;rieur et inf&#x000e9;rieur. Les autres muscles (notamment l&#x02019;infra-&#x000e9;pineux, le delto&#x000ef;de et le serratus ant&#x000e9;rieur) et les autres nerfs (long thoracique, axillaire, suprascapulaire, m&#x000e9;dian et ulnaire) &#x000e9;taient normaux. Les IRM du rachis cervical et du plexus brachial r&#x000e9;alis&#x000e9;es &#x000e0; sept mois du d&#x000e9;but de l&#x02019;infection n&#x02019;ont pas retrouv&#x000e9; de signal inflammatoire en regard de la onzi&#x000e8;me paire cr&#x000e2;nienne. N&#x000e9;anmoins, des stigmates de d&#x000e9;nervation du trap&#x000e8;ze droit, associant amyotrophie avec involution graisseuse et hypersignal STIR ont &#x000e9;t&#x000e9; retrouv&#x000e9;s, en faveur d&#x02019;un processus neurog&#x000e8;ne semi-r&#x000e9;cent dans le territoire spinal distal, compatible avec une atteinte datant de plus de 3&#x000a0;mois. Les autres s&#x000e9;rologies virales (h&#x000e9;patites B, C et E, VIH, CMV, Parvovirus B19) &#x000e9;taient n&#x000e9;gatives. Les radiographies standard ont infirm&#x000e9; la paralysie diaphragmatique. Les IRM du rachis cervical et de l&#x02019;&#x000e9;paule droite ont &#x000e9;limin&#x000e9; les autres diagnostics diff&#x000e9;rentiels neurologiques et ost&#x000e9;o-articulaires.<fig id="fig0005"><label>Fig. 1</label><caption><p>a, b. Ces images du premier patient illustrent l&#x02019;amyotrophie du trap&#x000e8;ze sup&#x000e9;rieur droit, avec une pseudo-atrophie de la fosse sus-&#x000e9;pineuse et un d&#x000e9;collement scapulaire&#x000a0;: la scapula est lat&#x000e9;ralis&#x000e9;e, &#x000e9;loign&#x000e9;e de la colonne, contrairement &#x000e0; ce qu&#x02019;on observe dans les d&#x000e9;collements scapulaires sur paralysie du serratus ant&#x000e9;rieur, qui sont plus marqu&#x000e9;s, avec une scapula m&#x000e9;diale, proche de la colonne vert&#x000e9;brale <xref rid="bib0125" ref-type="bibr">[11]</xref>, <xref rid="bib0130" ref-type="bibr">[12]</xref>.</p></caption><graphic xlink:href="gr1_lrg"/></fig><fig id="fig0010"><label>Fig. 2</label><caption><p>Trac&#x000e9; de l&#x02019;&#x000e9;tude de conduction nerveuse du NSA du cas 1&#x000a0;qui d&#x000e9;montre une importante l&#x000e9;sion axonale du NSA droit. On peut voir que les latences du c&#x000f4;t&#x000e9; droit sont similaires &#x000e0; celles du c&#x000f4;t&#x000e9; gauche normal, alors que l&#x02019;amplitude des potentiels d&#x02019;action moteurs est tr&#x000e8;s basse&#x000a0;: 2,4&#x000a0;mV vs 7,5&#x000a0;mV pour le muscle trap&#x000e8;ze (MT) sup&#x000e9;rieur, et 0,5&#x000a0;mV vs 5,1&#x000a0;mV pour le muscle trap&#x000e8;ze (MT) inf&#x000e9;rieur, ce qui correspond respectivement &#x000e0; une perte axonale de 66&#x000a0;% et 90&#x000a0;% <xref rid="bib0125" ref-type="bibr">[11]</xref>.</p></caption><graphic xlink:href="gr2_lrg"/></fig></p></sec><sec id="sec0020"><label>2.2</label><title>Cas 2</title><p id="par0040">Un patient de 74&#x000a0;ans diab&#x000e9;tique a pr&#x000e9;sent&#x000e9; un SRAS sur infection &#x000e0; COVID-19&#x000a0;document&#x000e9;e par PCR positive sur &#x000e9;couvillonnage naso-pharyng&#x000e9; et scanner thoracique. Son &#x000e9;tat a n&#x000e9;cessit&#x000e9; une prise en charge en r&#x000e9;animation, avec s&#x000e9;dation et intubation oro-trach&#x000e9;ale pendant 5&#x000a0;semaines. Pour l&#x02019;infection &#x000e0; COVID-19, il a &#x000e9;t&#x000e9; trait&#x000e9; par Cefotaxime, Dexamethasone et Tocilizumab. Son &#x000e9;tat s&#x02019;est secondairement compliqu&#x000e9; d&#x02019;une t&#x000e9;trapar&#x000e9;sie acquise de r&#x000e9;animation, transitoire, pr&#x000e9;dominant aux membres inf&#x000e9;rieurs. Une semaine apr&#x000e8;s sa sortie du service de r&#x000e9;animation, il s&#x02019;est plaint de douleurs et d&#x02019;un d&#x000e9;ficit moteur de l&#x02019;&#x000e9;paule gauche. L&#x02019;examen clinique a retrouv&#x000e9; une amyotrophie du trap&#x000e8;ze sup&#x000e9;rieur et des fosses supra et infra-&#x000e9;pineuses <xref rid="bib0120" ref-type="bibr">[10]</xref>, <xref rid="bib0125" ref-type="bibr">[11]</xref>. Les amplitudes articulaires actives de l&#x02019;&#x000e9;paule gauche &#x000e9;taient limit&#x000e9;es, particuli&#x000e8;rement l&#x02019;&#x000e9;l&#x000e9;vation lat&#x000e9;rale. L&#x02019;examen clinique a d&#x000e9;voil&#x000e9; un d&#x000e9;ficit musculaire important du trap&#x000e8;ze, une bascule lat&#x000e9;rale de la scapula au repos, et un abaissement avec glissement lat&#x000e9;ral brutal de la scapula lors des mouvements d&#x02019;&#x000e9;l&#x000e9;vation lat&#x000e9;rale du membre sup&#x000e9;rieur gauche <xref rid="bib0125" ref-type="bibr">[11]</xref>, <xref rid="bib0130" ref-type="bibr">[12]</xref>. Les r&#x000e9;flexes ost&#x000e9;o-tendineux &#x000e9;taient conserv&#x000e9;s, bilat&#x000e9;raux et sym&#x000e9;triques. L&#x02019;ENMG r&#x000e9;alis&#x000e9; 4&#x000a0;mois apr&#x000e8;s le d&#x000e9;but de l&#x02019;infection &#x000e0; COVID-19&#x000a0;a document&#x000e9; une atteinte axonale importante et isol&#x000e9;e du NSA&#x000a0;: l&#x02019;&#x000e9;tude des vitesses de conduction nerveuse a retrouv&#x000e9; des latences normales, mais une diminution importante de l&#x02019;amplitude des potentiels d&#x02019;action moteurs, sur les muscles trap&#x000e8;zes sup&#x000e9;rieur et inf&#x000e9;rieur gauches, en comparaison au c&#x000f4;t&#x000e9; droit. L&#x02019;examen &#x000e0; l&#x02019;aiguille a retrouv&#x000e9; des trac&#x000e9;s fortement neurog&#x000e8;nes et des signes de d&#x000e9;nervation active dans les muscles trap&#x000e8;zes sup&#x000e9;rieur et inf&#x000e9;rieur gauches. Les autres muscles (en particulier le sous-&#x000e9;pineux, sus-&#x000e9;pineux, serratus ant&#x000e9;rieur et sterno-cl&#x000e9;&#x000ef;do-masto&#x000ef;dien) et autres nerfs (long thoracique, axillaire, suprascapulaire, m&#x000e9;dian et ulnaire) &#x000e9;taient normaux. L&#x02019;IRM du plexus brachial r&#x000e9;alis&#x000e9;e un mois plus tard a mis en &#x000e9;vidence une amyotrophie du trap&#x000e8;ze gauche en s&#x000e9;quence T1, avec hypersignal en s&#x000e9;quence STIR, en faveur d&#x02019;une d&#x000e9;nervation semi-r&#x000e9;cente dans le territoire distal du XIe nerf cr&#x000e2;nien gauche. L&#x02019;IRM cervicale a confirm&#x000e9; l&#x02019;absence de l&#x000e9;sion des ganglions spinaux, des racines post-ganglionnaires et des troncs plexiques et a &#x000e9;cart&#x000e9; les &#x000e9;tiologies rachidiennes et endo-canalaires. L&#x02019;IRM de l&#x02019;&#x000e9;paule gauche a retrouv&#x000e9; une fissure profonde de la portion distale du tendon sous-&#x000e9;pineux, ainsi qu&#x02019;une enth&#x000e9;sopathie du sus-&#x000e9;pineux. La paralysie diaphragmatique a &#x000e9;t&#x000e9; &#x000e9;cart&#x000e9;e par radiographies. Le bilan s&#x000e9;rologique n&#x02019;a trouv&#x000e9; aucune autre infection virale r&#x000e9;cente (h&#x000e9;patites B, C et E, VIH, CMV, Parvovirus B19).</p></sec></sec><sec id="sec0025"><label>3</label><title>Discussion</title><p id="par0045">Le NSA est la branche terminale externe de la onzi&#x000e8;me paire cr&#x000e2;nienne. Il traverse la r&#x000e9;gion cervicale, pour innerver le muscle sterno-cl&#x000e9;&#x000ef;do-masto&#x000ef;dien (SCM) et les trois faisceaux du muscle trap&#x000e8;ze <xref rid="bib0125" ref-type="bibr">[11]</xref>, <xref rid="bib0130" ref-type="bibr">[12]</xref>. Les cons&#x000e9;quences d&#x02019;une l&#x000e9;sion du NSA sont une atrophie et une faiblesse du muscle trap&#x000e8;ze, responsables d&#x02019;une limitation de l&#x02019;&#x000e9;l&#x000e9;vation lat&#x000e9;rale du membre sup&#x000e9;rieur, avec une bascule lat&#x000e9;rale de la scapula au repos et une dyskin&#x000e9;sie scapulo-thoracique, sous la forme d&#x02019;un glissement brutal lat&#x000e9;ral de la scapula lors des mouvements d&#x02019;&#x000e9;l&#x000e9;vation lat&#x000e9;rale du bras. Cette pr&#x000e9;sentation clinique typique a &#x000e9;t&#x000e9; retrouv&#x000e9;e dans les deux cas rapport&#x000e9;s ci-dessus. La force musculaire du SCM &#x000e9;tait pr&#x000e9;serv&#x000e9;e, comme cela est habituel au cours des paralysies du NSA li&#x000e9; &#x000e0; une NAPT <xref rid="bib0120" ref-type="bibr">[10]</xref>, <xref rid="bib0125" ref-type="bibr">[11]</xref>, <xref rid="bib0130" ref-type="bibr">[12]</xref>.</p><p id="par0050">Trois cas de n&#x000e9;vralgie amyotrophiante ont d&#x000e9;j&#x000e0; &#x000e9;t&#x000e9; rapport&#x000e9;s, dans les suites d&#x02019;une infection &#x000e0; COVID-19 <xref rid="bib0095" ref-type="bibr">[5]</xref>, <xref rid="bib0100" ref-type="bibr">[6]</xref>, <xref rid="bib0105" ref-type="bibr">[7]</xref>. Un cas &#x000e9;tait purement sensitif <xref rid="bib0095" ref-type="bibr">[5]</xref> et n&#x02019;impliquait que le nerf cutan&#x000e9; antebrachial lat&#x000e9;rale Un autre cas impliquait les muscles sus-&#x000e9;pineux, sous-&#x000e9;pineux, grand rond, petit rond et trap&#x000e8;ze. Les l&#x000e9;sions retrouv&#x000e9;es en IRM &#x000e9;taient typiques, mais il n&#x02019;y avait pas de r&#x000e9;sultats d&#x02019;ENMG disponibles <xref rid="bib0100" ref-type="bibr">[6]</xref>. Le troisi&#x000e8;me cas <xref rid="bib0105" ref-type="bibr">[7]</xref> &#x000e9;tait d&#x000e9;crit comme une l&#x000e9;sion du nerf m&#x000e9;dian avec un bloc de conduction partiel de l&#x02019;avant-bras et une absence de signes de d&#x000e9;nervation active retrouv&#x000e9;s &#x000e0; l&#x02019;examen &#x000e0; l&#x02019;aiguille.</p><p id="par0055">La n&#x000e9;vralgie amyotrophiante est habituellement un diagnostic d&#x02019;&#x000e9;limination <xref rid="bib0085" ref-type="bibr">[3]</xref>. Avant de l&#x02019;&#x000e9;voquer pour ces deux patients, nous avons &#x000e9;limin&#x000e9; les autres causes d&#x02019;atteinte potentielle du nerf spinal accessoire, gr&#x000e2;ce &#x000e0; l&#x02019;examen clinique, &#x000e0; l&#x02019;ENMG et aux IRM. Nous avons tout d&#x02019;abord &#x000e9;limin&#x000e9; les atteintes du rachis cervical et du plexus brachial pouvant &#x000ea;tre li&#x000e9;es aux mobilisations du patient, notamment lors des s&#x000e9;ances de d&#x000e9;cubitus ventral r&#x000e9;alis&#x000e9;es sous s&#x000e9;dation lors du s&#x000e9;jour en r&#x000e9;animation. L&#x02019;enqu&#x000ea;te anamnestique n&#x02019;a pas identifi&#x000e9; d&#x02019;ant&#x000e9;c&#x000e9;dent d&#x02019;abord veineux jugulaire, ni d&#x02019;ant&#x000e9;c&#x000e9;dent chirurgical de la r&#x000e9;gion cervicale, qui pourraient &#x000ea;tre une autre cause de l&#x000e9;sion du nerf spinal accessoire. Les deux patients d&#x000e9;crits ont en effet port&#x000e9; un cath&#x000e9;ter sous-clavier comme principale voie d&#x02019;abord vasculaire, lors de leur s&#x000e9;jour en r&#x000e9;animation. Celui-ci passait &#x000e0; distance du trajet du nerf spinal accessoire. L&#x02019;un des deux avait &#x000e9;galement un cath&#x000e9;ter sous-clavier controlat&#x000e9;ral. Les deux patients souffraient par ailleurs d&#x02019;autres pathologies des membres sup&#x000e9;rieurs, facteurs confondants du diagnostic de NAPT, mais ne permettant pas d&#x02019;expliquer la totalit&#x000e9; du tableau clinique. Dans le premier cas, le patient souffrait d&#x02019;une capsulite r&#x000e9;tractile de l&#x02019;&#x000e9;paule, qui expliquait la limitation des amplitudes articulaires passives. Cette association NAPT et capsulite r&#x000e9;tractile est d&#x000e9;crite dans 17&#x000a0;% des cas de NAPT <xref rid="bib0085" ref-type="bibr">[3]</xref>. Le second patient souffrait d&#x02019;une tendinopathie de la coiffe des rotateurs, pathologie tr&#x000e8;s fr&#x000e9;quente dans sa tranche d&#x02019;&#x000e2;ge, participant certainement &#x000e0; la symptomatologie douloureuse de l&#x02019;&#x000e9;paule, mais n&#x02019;expliquant pas l&#x02019;amyotrophie des fosses sus et sous-&#x000e9;pineuses, en l&#x02019;absence de rupture tendineuse. Ce patient a &#x000e9;galement souffert d&#x02019;une neuromyopathie de r&#x000e9;animation qui a pu initialement expliquer une partie du d&#x000e9;ficit moteur de l&#x02019;&#x000e9;paule. Enfin, la pr&#x000e9;sentation clinique des deux patients d&#x000e9;crits ci-dessus &#x000e9;tait typique de NAPT et les diagnostics bien document&#x000e9;s.</p><p id="par0060">La paralysie du NSA est sous-diagnostiqu&#x000e9;e, particuli&#x000e8;rement lorsqu&#x02019;elle est isol&#x000e9;e <xref rid="bib0125" ref-type="bibr">[11]</xref>. Elle est retrouv&#x000e9;e par Van Alfen et al. dans 20&#x000a0;% des cas de n&#x000e9;vralgie amyotrophiante <xref rid="bib0085" ref-type="bibr">[3]</xref>. L&#x02019;amyotrophie des fosses sus et sous-&#x000e9;pineuses ainsi que le d&#x000e9;collement scapulaire peuvent &#x000e9;voquer cliniquement une atteinte du nerf suprascapulaire ou du nerf long thoracique. Cela peut expliquer pourquoi la NAPT reste souvent non diagnostiqu&#x000e9;e. Les atteintes du syst&#x000e8;me nerveux p&#x000e9;riph&#x000e9;rique post-infection &#x000e0; COVID-19&#x000a0;semblent rares. Leur incidence varie entre 0,05&#x000a0;% et 8,9&#x000a0;% <xref rid="bib0080" ref-type="bibr">[2]</xref>, <xref rid="bib0135" ref-type="bibr">[13]</xref>. Ce sont principalement des syndromes de Guillain-Barr&#x000e9; (ou des variantes) <xref rid="bib0080" ref-type="bibr">[2]</xref>. La NAPT est probablement sous-diagnostiqu&#x000e9;e car le handicap fonctionnel associ&#x000e9; reste g&#x000e9;n&#x000e9;ralement l&#x000e9;ger et limit&#x000e9; <xref rid="bib0085" ref-type="bibr">[3]</xref>. Le fait que la COVID-19&#x000a0;puisse &#x000ea;tre responsable du SGB est un argument en faveur de sa possible imputabilit&#x000e9; dans la n&#x000e9;vralgie amyotrophiante, puisque leur physiopathologie semble tr&#x000e8;s proche, et que les m&#x000ea;mes virus peuvent d&#x000e9;clencher les deux pathologies <xref rid="bib0085" ref-type="bibr">[3]</xref>, <xref rid="bib0090" ref-type="bibr">[4]</xref>, <xref rid="bib0070" ref-type="bibr">[14]</xref>.</p><p id="par0065">Il existe cependant deux limitations principales dans notre travail. La premi&#x000e8;re est l&#x02019;impossibilit&#x000e9; de d&#x000e9;terminer exactement le d&#x000e9;lai &#x000e9;coul&#x000e9; entre l&#x02019;infection &#x000e0; COVID-19&#x000a0;et la survenue de la NAPT&#x000a0;: les patients hospitalis&#x000e9;s en r&#x000e9;animation &#x000e9;taient en effet dans l&#x02019;incapacit&#x000e9; de communiquer. Par ailleurs, &#x000e0; la sortie du service de r&#x000e9;animation, les patients pr&#x000e9;sentaient de multiples d&#x000e9;ficiences <xref rid="bib0115" ref-type="bibr">[9]</xref>, notamment cognitives. Il a fallu attendre qu&#x02019;ils aient retrouv&#x000e9; une autonomie et des capacit&#x000e9;s suffisantes d&#x02019;orientation et de communication pour prendre conscience de leur handicap et se plaindre de leur &#x000e9;paule. La seconde limitation est le fait que la NAPT peut &#x000ea;tre d&#x000e9;clench&#x000e9;e par un traumatisme ou une intervention chirurgicale. Les soins intensifs pourraient ainsi s&#x02019;av&#x000e9;rer un traumatisme suffisant pour d&#x000e9;clencher une NAPT. Mais une recherche sur Medline n&#x02019;a pas permis de retrouver de cas similaire dans la litt&#x000e9;rature.</p></sec><sec id="sec0030"><title>D&#x000e9;claration de liens d&#x02019;int&#x000e9;r&#x000ea;ts</title><p id="par0070">Les auteurs d&#x000e9;clarent ne pas avoir de liens d&#x02019;int&#x000e9;r&#x000ea;ts.</p></sec></body><back><ref-list id="bibl0005"><title>R&#x000e9;f&#x000e9;rences</title><ref id="bib0075"><label>1</label><element-citation publication-type="journal" id="sbref0075"><person-group person-group-type="author"><name><surname>Yachou</surname><given-names>Y.</given-names></name><name><surname>El Idrissi</surname><given-names>A.</given-names></name><name><surname>Belapasov</surname><given-names>V.</given-names></name><etal/></person-group><article-title>Neuroinvasion, neurotropic, and neuroinflammatory events of SARS-CoV-2: understanding the neurological manifestations in COVID-19&#x000a0;patients</article-title><source>Neurol Sci</source><volume>41</volume><year>2020</year><fpage>2657</fpage><lpage>2669</lpage><pub-id pub-id-type="pmid">32725449</pub-id></element-citation></ref><ref id="bib0080"><label>2</label><element-citation publication-type="journal" id="sbref0080"><person-group person-group-type="author"><name><surname>Ellul</surname><given-names>M.A.</given-names></name><name><surname>Benjamin</surname><given-names>L.</given-names></name><name><surname>Singh</surname><given-names>B.</given-names></name><etal/></person-group><article-title>Neurological associations of COVID-19</article-title><source>Lancet Neurol</source><volume>19</volume><year>2020</year><fpage>767</fpage><lpage>783</lpage><pub-id pub-id-type="pmid">32622375</pub-id></element-citation></ref><ref id="bib0085"><label>3</label><element-citation publication-type="journal" id="sbref0085"><person-group person-group-type="author"><name><surname>van Alfen</surname><given-names>N.</given-names></name><name><surname>van Engelen</surname><given-names>B.G.</given-names></name></person-group><article-title>The clinical spectrum of neuralgic amyotrophy in 246&#x000a0;cases</article-title><source>Brain</source><volume>129</volume><year>2006</year><fpage>438</fpage><lpage>450</lpage><pub-id pub-id-type="pmid">16371410</pub-id></element-citation></ref><ref id="bib0090"><label>4</label><element-citation publication-type="journal" id="sbref0090"><person-group person-group-type="author"><name><surname>Vighetto</surname><given-names>A.</given-names></name><name><surname>Grand</surname><given-names>C.</given-names></name><name><surname>Confavreux</surname><given-names>C.</given-names></name><etal/></person-group><article-title>The Parsonage-Turner syndrome and similar diseases. 29&#x000a0;cases</article-title><source>Rev Neurol</source><volume>144</volume><year>1988</year><fpage>494</fpage><lpage>498</lpage><pub-id pub-id-type="pmid">3187305</pub-id></element-citation></ref><ref id="bib0095"><label>5</label><element-citation publication-type="journal" id="sbref0095"><person-group person-group-type="author"><name><surname>Cacciavillani</surname><given-names>M.</given-names></name><name><surname>Salvalaggio</surname><given-names>A.</given-names></name><name><surname>Briani</surname><given-names>C.</given-names></name></person-group><article-title>Pure sensory neuralgic amyotrophy in COVID-19&#x000a0;infection</article-title><source>Muscle Nerve</source><volume>63</volume><year>2020</year><fpage>E7</fpage><lpage>E8</lpage><pub-id pub-id-type="pmid">33001471</pub-id></element-citation></ref><ref id="bib0100"><label>6</label><element-citation publication-type="journal" id="sbref0100"><person-group person-group-type="author"><name><surname>Mitry</surname><given-names>M.A.</given-names></name><name><surname>Collins</surname><given-names>L.K.</given-names></name><name><surname>Kazam</surname><given-names>J.J.</given-names></name><etal/></person-group><article-title>Parsonage-turner syndrome associated with SARS-CoV2 (COVID-19) infection</article-title><source>Clin Imag</source><volume>72</volume><year>2020</year><fpage>8</fpage><lpage>10</lpage></element-citation></ref><ref id="bib0105"><label>7</label><element-citation publication-type="journal" id="sbref0105"><person-group person-group-type="author"><name><surname>Siepmann</surname><given-names>T.</given-names></name><name><surname>Kitzler</surname><given-names>H.H.</given-names></name><name><surname>Lueck</surname><given-names>C.</given-names></name><etal/></person-group><article-title>Neuralgic amyotrophy following infection with SARS-CoV-2</article-title><source>Muscle Nerve</source><volume>62</volume><year>2020</year><fpage>E68</fpage><lpage>E70</lpage><pub-id pub-id-type="pmid">32710672</pub-id></element-citation></ref><ref id="bib0110"><label>8</label><element-citation publication-type="journal" id="sbref0110"><person-group person-group-type="author"><name><surname>Varatharaj</surname><given-names>A.</given-names></name><name><surname>Thomas</surname><given-names>N.</given-names></name><name><surname>Ellul</surname><given-names>M.A.</given-names></name><etal/></person-group><article-title>Neurological and neuropsychiatric complications of COVID-19&#x000a0;in 153&#x000a0;patients: a UK-wide surveillance study</article-title><source>Lancet</source><volume>7</volume><year>2020</year><fpage>875</fpage><lpage>882</lpage></element-citation></ref><ref id="bib0115"><label>9</label><element-citation publication-type="journal" id="sbref0115"><person-group person-group-type="author"><name><surname>Kotfis</surname><given-names>K.</given-names></name><name><surname>Williams Roberson</surname><given-names>S.</given-names></name><name><surname>Wilson</surname><given-names>J.E.</given-names></name><etal/></person-group><article-title>COVID-19: ICU delirium management during SARS-CoV-2&#x000a0;pandemic</article-title><source>Critical Care (London, England)</source><volume>24</volume><year>2020</year><fpage>176</fpage></element-citation></ref><ref id="bib0120"><label>10</label><element-citation publication-type="journal" id="sbref0120"><person-group person-group-type="author"><name><surname>Seror</surname><given-names>P.</given-names></name><name><surname>Roren</surname><given-names>A.</given-names></name><name><surname>Lefevre-Colau</surname><given-names>M.M.</given-names></name></person-group><article-title>Infraspinatus muscle palsy involving suprascapular nerve, brachial plexus or cervical roots related to inflammatory or mechanical causes: Experience of 114&#x000a0;cases</article-title><source>NCCN</source><volume>50</volume><year>2020</year><fpage>103</fpage><lpage>111</lpage></element-citation></ref><ref id="bib0125"><label>11</label><element-citation publication-type="journal" id="sbref0125"><person-group person-group-type="author"><name><surname>Seror</surname><given-names>P.</given-names></name><name><surname>Stojkovic</surname><given-names>T.</given-names></name><name><surname>Lefevre-Colau</surname><given-names>M.M.</given-names></name><etal/></person-group><article-title>Diagnosis of unilateral trapezius muscle palsy: 54&#x000a0;Cases</article-title><source>Muscle Nerve</source><volume>56</volume><year>2017</year><fpage>215</fpage><lpage>223</lpage><pub-id pub-id-type="pmid">27864983</pub-id></element-citation></ref><ref id="bib0130"><label>12</label><element-citation publication-type="journal" id="sbref0130"><person-group person-group-type="author"><name><surname>Seror</surname><given-names>P.</given-names></name><name><surname>Lenglet</surname><given-names>T.</given-names></name><name><surname>Nguyen</surname><given-names>C.</given-names></name><etal/></person-group><article-title>Unilateral winged scapula: clinical and electrodiagnostic experience with 128&#x000a0;cases, with special attention to long thoracic nerve palsy</article-title><source>Muscle Nerve</source><volume>57</volume><year>2018</year><fpage>913</fpage><lpage>920</lpage><pub-id pub-id-type="pmid">29314072</pub-id></element-citation></ref><ref id="bib0135"><label>13</label><element-citation publication-type="journal" id="sbref0135"><person-group person-group-type="author"><name><surname>Mao</surname><given-names>L.</given-names></name><name><surname>Jin</surname><given-names>H.</given-names></name><name><surname>Wang</surname><given-names>M.</given-names></name><etal/></person-group><article-title>Neurologic manifestations of hospitalized patients with coronavirus disease 2019&#x000a0;in Wuhan, China</article-title><source>JAMA Neurol</source><volume>77</volume><year>2020</year><fpage>683</fpage><lpage>690</lpage><pub-id pub-id-type="pmid">32275288</pub-id></element-citation></ref><ref id="bib0070"><label>14</label><element-citation publication-type="journal" id="sbref0070"><person-group person-group-type="author"><name><surname>van Eijk</surname><given-names>J.J.</given-names></name><name><surname>van Alfen</surname><given-names>N.</given-names></name><name><surname>Tio-Gillen</surname><given-names>A.P.</given-names></name><etal/></person-group><article-title>Screening for antecedent Campylobacter jejuni infections and anti-ganglioside antibodies in idiopathic neuralgic amyotrophy</article-title><source>J Peripher Nerv Syst</source><volume>16</volume><year>2011</year><fpage>153</fpage><lpage>156</lpage><pub-id pub-id-type="pmid">21692917</pub-id></element-citation></ref></ref-list><fn-group><fn id="d36e398"><label>&#x02606;</label><p id="npar0005">Ne pas utiliser, pour citation, la r&#x000e9;f&#x000e9;rence fran&#x000e7;aise de cet article mais la r&#x000e9;f&#x000e9;rence anglaise de <italic>Joint Bone Spine</italic> avec le DOI ci-dessus.</p></fn></fn-group></back></article>